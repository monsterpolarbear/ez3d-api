export class api {

    iframeEl: HTMLIFrameElement;
    iframeLoaded: boolean = false;
    onEnterAllow:boolean = true;
    private triggersOnClick: NodeList;
    private triggersOnEnter: NodeList;


    constructor(iframEl: HTMLIFrameElement) {
        this.iframeEl = iframEl;

        this.iframeEl.addEventListener('load', this.setReadyState);

        this.collectElements();
        this.attachListeners();
        this.createObserver();

    }

    setReadyState = () => {

        this.iframeLoaded = true;
        this.iframeEl.removeEventListener('load', this.setReadyState);

    }


    collectElements() {
        this.triggersOnClick =
            document.querySelectorAll('[data-ez3d-onclick]');

        this.triggersOnEnter =
            document.querySelectorAll('[data-ez3d-onenter]');
    }


    handleClick = (e) => {

        if (!this.iframeLoaded) return;
        if (!this.iframeEl?.contentWindow) return;

        //  get data attribute
        const shot = e.target.dataset.ez3dOnclick

        if (!shot) return;
        // postmessage to iframe
        this.iframeEl.contentWindow.postMessage({ type: "shot", data: shot }, this.iframeEl.src);

    }


    // Attach event listeners to triggers
    attachListeners() {
        this.triggersOnClick.forEach(el => {
            el.addEventListener('click', this.handleClick);

        })
    }




    createObserver() {
        if (!this.triggersOnEnter.length) return

        const observerOptions = {
            rootMargin: "0px",
            threshold: 0.3,
        };

        let observer = new IntersectionObserver(observerCallback.bind(this), observerOptions);

        function observerCallback(entries, observer) {
            entries.forEach((entry) => {
                if (entry.isIntersecting) {
                    if (!this.iframeLoaded) return;
                    if (!this.iframeEl?.contentWindow) return;
                    if (!this.onEnterAllow) return;

                    const shot = entry.target.dataset.ez3dOnenter

                    if (!shot) return;
                    this.iframeEl.contentWindow.postMessage({ type: "shot", data: shot }, this.iframeEl.src);
                }
            })
        }

        this.triggersOnEnter.forEach(el => {
            observer.observe(el);
        })

    }

    /**
     * Trigger a shot
     */
    shot(shotid: string) {
        if (!this.iframeLoaded) return;
        if (!this.iframeEl?.contentWindow) return;
        if (!shotid) return;
        this.iframeEl.contentWindow.postMessage({ type: "shot", data: shotid }, this.iframeEl.src);
    }


    arButton(state: string) {
        if (!this.iframeLoaded) return;
        if (!this.iframeEl?.contentWindow) return;


        switch (state) {
            case 'show':
                this.iframeEl.contentWindow.postMessage({ type: "ar-button", data: 'show' }, this.iframeEl.src);
                break;
            case 'hide':
                this.iframeEl.contentWindow.postMessage({ type: "ar-button", data: 'hide' }, this.iframeEl.src);
                break;
            default:
                console.warn("Wrong state for ar-button. use 'show' or 'hide'.");

        }
    }

}


